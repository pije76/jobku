from django.conf.urls import url
from custom_rest_auth.views import (
    CustomLoginView, CustomLogoutView, CustomUserDetailsView, CustomPasswordChangeView,
    CustomPasswordResetView, CustomPasswordResetConfirmView
)

urlpatterns = [
    url(r'^password/reset/$', CustomPasswordResetView.as_view(),
        name='rest_password_reset'),
    url(r'^password/reset/confirm/$', CustomPasswordResetConfirmView.as_view(),
        name='rest_password_reset_confirm'),
    url(r'^login/$', CustomLoginView.as_view(), name='rest_login'),
    url(r'^logout/$', CustomLogoutView.as_view(), name='rest_logout'),
    url(r'^user/$', CustomUserDetailsView.as_view(), name='rest_user_details'),
    url(r'^password/change/$', CustomPasswordChangeView.as_view(),
        name='rest_password_change'),
]
