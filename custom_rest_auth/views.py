from rest_auth.views import (
    LoginView, LogoutView, UserDetailsView, PasswordChangeView,
    PasswordResetView, PasswordResetConfirmView
)
from rest_framework_tracking.mixins import LoggingMixin


class CustomLoginView(LoggingMixin, LoginView):
    pass


class CustomLogoutView(LoggingMixin, LogoutView):
    pass


class CustomUserDetailsView(LoggingMixin, UserDetailsView):
    pass


class CustomPasswordResetView(LoggingMixin, PasswordResetView):
    pass


class CustomPasswordResetConfirmView(LoggingMixin, PasswordResetConfirmView):
    sensitive_fields = {'new_password1', 'new_password2'}


class CustomPasswordChangeView(LoggingMixin, PasswordChangeView):
    sensitive_fields = {'new_password1', 'new_password2', 'old_password'}
