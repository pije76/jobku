from rest_auth.registration.views import RegisterView, VerifyEmailView
from django.views.generic import TemplateView
from rest_framework_tracking.mixins import LoggingMixin


class CustomRegisterView(LoggingMixin, RegisterView):
    sensitive_fields = {'password1', 'password2'}


class CustomVerifyEmailView(LoggingMixin, VerifyEmailView):
    pass


class CustomTemplateView(LoggingMixin, TemplateView):
    pass
