from django.conf.urls import url
from custom_rest_auth.registration.views import CustomRegisterView, CustomVerifyEmailView, CustomTemplateView

urlpatterns = [
    url(r'^$', CustomRegisterView.as_view(), name='rest_register'),
    url(r'^verify-email/$', CustomVerifyEmailView.as_view(), name='rest_verify_email'),
    url(r'^account-confirm-email/(?P<key>[-:\w]+)/$', CustomTemplateView.as_view(),
        name='account_confirm_email'),
]
