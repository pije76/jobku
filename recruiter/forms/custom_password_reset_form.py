from __future__ import unicode_literals

from django.contrib.auth import (
    get_user_model,
)
from django.contrib.auth.forms import PasswordResetForm
from django.core.mail import EmailMultiAlternatives
from django.template import loader
from django.conf import settings

UserModel = get_user_model()


class CustomPasswordResetForm(PasswordResetForm):

    def send_mail(self, subject_template_name, email_template_name,
                  context, from_email, to_email, html_email_template_name=None):
        """
        Sends a django.core.mail.EmailMultiAlternatives to `to_email`.
        """
        subject = loader.render_to_string(subject_template_name, context)
        # Email subject *must not* contain newlines
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string(email_template_name, context)

        email_message = EmailMultiAlternatives(subject, body, from_email, [to_email])
        context['reset_url'] = f"{settings.SITE_NAME}forget/{context['uid'].decode('utf-8')}/{context['token']}"
        html_email = loader.render_to_string("registration/html_password_reset_email.html", context)
        email_message.attach_alternative(html_email, 'text/html')
        email_message.send()
