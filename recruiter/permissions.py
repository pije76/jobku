from rest_framework.permissions import IsAuthenticated, BasePermission
from recruiter.models.recruiter import Blacklist


class IsOwner(IsAuthenticated):
    def has_object_permission(self, request, view, obj):
        return request.user == obj

class BlacklistPermission(BasePermission):

    def has_permission(self, request, view):
        ip_addr = request.META['REMOTE_ADDR']
        blacklisted = Blacklist.objects.filter(ip_addr=ip_addr).exists()
        return not blacklisted
