from rest_framework import serializers
from recruiter.serializers.recruiter_login_serializer import RecruiterLoginSerializer


class JWTSerializer(serializers.Serializer):
    """
    Serializer for JWT authentication.
    """
    token = serializers.SerializerMethodField()
    user = RecruiterLoginSerializer(many=False)

    def get_token(self, obj):
        return obj['token']
