from django.contrib.auth import get_user_model
from django.conf import settings
from recruiter.forms.custom_password_reset_form import CustomPasswordResetForm
from rest_auth.serializers import PasswordResetSerializer

# Get the UserModel
UserModel = get_user_model()


class CustomPasswordResetSerializer(PasswordResetSerializer):

    """
    Serializer for requesting a password reset e-mail.
    """

    password_reset_form_class = CustomPasswordResetForm

    def save(self):
        request = self.context.get('request')
        # Set some values to trigger the send_email method.
        opts = {
            'use_https': request.is_secure(),
            'from_email': getattr(settings, 'DEFAULT_FROM_EMAIL'),
            'request': request,
        }

        opts.update(self.get_email_options())
        self.reset_form.save(**opts)
