from rest_framework import serializers
from rest_auth.registration.serializers import RegisterSerializer


class CustomRegisterSerializer(RegisterSerializer):
    contact_number = serializers.CharField(required=False, write_only=True)

    def get_cleaned_data(self):
        return {
            'password1': self.validated_data.get('password1', ''),
            'email': self.validated_data.get('email', ''),
            'contact_number': self.validated_data.get('contact_number', '')
        }

    def custom_signup(self, request, user):
        user.contact_number = self.get_cleaned_data()['contact_number']
        user.save()
        return user
