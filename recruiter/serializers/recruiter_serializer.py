from rest_framework import serializers
from recruiter.models import Recruiter
from referral.models import Referral
from rest_framework_serializer_extensions.fields import HashIdField
from django.utils import timezone
from django.db.models.functions import TruncMonth, ExtractMonth
from django.db.models import Count
from referral.models.referral import STATUS
from collections import Counter, OrderedDict


class ProfileSerializer(serializers.ModelSerializer):
    id = HashIdField(model=Recruiter, read_only=True)
    email = serializers.EmailField(read_only=True)

    class Meta:
        model = Recruiter
        exclude = ('password',)

class RecruiterSerializer(serializers.ModelSerializer):
    id = HashIdField(model=Recruiter, read_only=True)
    statistic = serializers.SerializerMethodField()

    def get_statistic(self, obj):
        statistic_count = obj.referral_set.values('status').annotate(count=Count('id'))
        status = dict(STATUS)
        for x in statistic_count:
            x['status'] = status[x['status']]
        return statistic_count

    class Meta:
        model = Recruiter
        fields = ('id', 'username', 'first_name', 'last_name', 'statistic',)

class UserDetailsSerializer(serializers.ModelSerializer):
    pk = HashIdField(model=Recruiter, read_only=True)
    email_sent_today = serializers.SerializerMethodField()
    referral_per_month = serializers.SerializerMethodField()
    rank = serializers.SerializerMethodField()
    hires = serializers.SerializerMethodField()
    referred_skill = serializers.SerializerMethodField()

    def get_email_sent_today(self, obj):
        return obj.referral_set.filter(date_created__date=timezone.now().date(), type__in=[0, 2]).count()

    def get_referral_per_month(self, obj):
        return obj.referral_set.annotate(month=ExtractMonth('date_created')).values('month').annotate(count=Count('id')).values('month', 'count')

    def get_rank(self, obj):
        for idx, x in enumerate(self.Meta.model.objects.annotate(num_referral=Count('referral')).order_by('-num_referral')):
            if x.id == obj.id:
                rank = idx + 1
                break
        top_percentage = rank / self.Meta.model.objects.count() * 100
        top_list = [5, 10, 20, 30, 40]
        top_percentage_round = None
        for x in top_list:
            if top_percentage <= x:
                top_percentage_round = x
                break
        return {
            'rank': rank,
            'top_percentage_round': top_percentage_round
        }

    def get_hires(self, obj):
        return {
            'success_hire': obj.referral_set.filter(status=3).count(),
            'this_month': obj.referral_set.filter(status=3, date_hired__month=timezone.now().month).count(),
            'rejected_hire': obj.referral_set.filter(status=2).count()
        }

    def get_referred_skill(self, obj):
        skill = list()
        for obj in obj.referral_set.values_list('skill', flat=True):
            try:
                skill.extend(obj)
            except TypeError:
                pass
        return OrderedDict(Counter(skill).most_common(3))

    class Meta:
        model = Recruiter
        fields = ('pk', 'username', 'email', 'first_name', 'last_name', 'contact_number', 'skype', 'commission', 'city', 'country', 'date_of_birth', 'occupation', 'photo', 'have_recruited', 'email_sent_today', 'referral_per_month', 'rank', 'hires', 'referred_skill', 'company',)
        read_only_fields = ('email',)
