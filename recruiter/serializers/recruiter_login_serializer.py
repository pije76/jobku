from rest_framework import serializers
from recruiter.models.recruiter import Recruiter


class RecruiterLoginSerializer(serializers.ModelSerializer):
    class Meta:
        model = Recruiter
        fields = ('email', 'contact_number', 'onboarding',)
