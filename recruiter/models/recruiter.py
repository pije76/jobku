from django.contrib.auth.models import AbstractUser
from django.db import models
from django.core.validators import RegexValidator

WORKER_STATUS_PART_TIME = 0
WORKER_STATUS_FULL_TIME = 1
WORKER_STATUS = (
    (WORKER_STATUS_PART_TIME, 'Part-time'),
    (WORKER_STATUS_FULL_TIME, 'Full-time'),
)

EXPERIENCE_NO = 0
EXPERIENCE_INTERNAL = 1
EXPERIENCE_EXTERNAL = 2
EXPERIENCE = (
    (EXPERIENCE_NO, 'No experience'),
    (EXPERIENCE_INTERNAL, 'Internal recruiting'),
    (EXPERIENCE_EXTERNAL, 'External recruiting or head-hunting'),
)


class Recruiter(AbstractUser):
    photo = models.ImageField(upload_to='recruiter/%Y/%m/%d/', null=True, max_length=255)
    date_of_birth = models.DateField(max_length=8, null=True, blank=True)
    city = models.CharField(default='', max_length=100)
    country = models.CharField(default='', max_length=100)
    # Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message="Wrong Format")
    contact_number = models.CharField(validators=[phone_regex], max_length=17, blank=True)
    skype = models.CharField(max_length=100, blank=True)
    occupation = models.CharField(max_length=100, blank=True)
    commission = models.DecimalField(default=0, max_digits=8, decimal_places=2)
    worker_status = models.IntegerField(default=0, choices=WORKER_STATUS)
    company = models.CharField(default='', max_length=150)
    experience = models.IntegerField(default=0, choices=EXPERIENCE)
    bank_name = models.CharField(default='', max_length=100)
    bank_branch_code = models.CharField(default='', max_length=5)
    bank_swift_code = models.CharField(default='', max_length=100)
    bank_account_number = models.CharField(default='', max_length=25)
    bank_beneficiary_name = models.CharField(default='', max_length=50)
    have_recruited = models.BooleanField(default=False)
    onboarding = models.BooleanField(default=False)

    def __str__(self):
        return self.email

class Blacklist(models.Model):
    ip_addr = models.GenericIPAddressField()

    def __str__(self):
        return self.ip_addr
