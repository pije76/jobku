from rest_framework import viewsets, mixins
from rest_framework.views import APIView
from rest_framework.response import Response
from recruiter.models import Recruiter
from recruiter.serializers.recruiter_serializer import ProfileSerializer, RecruiterSerializer
from recruiter.permissions import IsOwner
from rest_framework_serializer_extensions.views import ExternalIdViewMixin
from rest_framework_tracking.mixins import LoggingMixin
from allauth.account.utils import send_email_confirmation
from django.db.models import Count


class ProfileViewSet(LoggingMixin,
                    mixins.CreateModelMixin,
                    mixins.RetrieveModelMixin,
                    mixins.UpdateModelMixin,
                    mixins.DestroyModelMixin,
                    viewsets.GenericViewSet):
    queryset = Recruiter.objects.all()
    serializer_class = ProfileSerializer
    permission_classes = (IsOwner,)

class RecruiterViewSet(LoggingMixin,
                    ExternalIdViewMixin,
                    mixins.ListModelMixin,
                    mixins.RetrieveModelMixin,
                    viewsets.GenericViewSet):
    queryset = Recruiter.objects.annotate(num_referral=Count('referral')).order_by('-num_referral')
    serializer_class = RecruiterSerializer

    def list(self, request):
        response = super().list(request)
        for idx, obj in enumerate(response.data):
            obj['rank'] = idx + 1
        return Response(response.data)

class ResendConfirmation(LoggingMixin, APIView):

    def post(self, request, format=None):
        try:
            recruiter = Recruiter.objects.get(email=request.data['email'])
            send_email_confirmation(request, recruiter)
        except Recruiter.DoesNotExist:
            pass
        return Response({"detail": "Verification e-mail sent."})
