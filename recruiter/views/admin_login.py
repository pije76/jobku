from django.contrib.auth import authenticate
from rest_framework import exceptions
from rest_auth.views import LoginView
from rest_framework_tracking.mixins import LoggingMixin


class AdminLogin(LoggingMixin, LoginView):
    """Admin login"""

    def post(self, request, *args, **kwargs):
        user = authenticate(
            email=self.request.data["email"],
            password=self.request.data["password"]
        )

        if (user is not None) and (not user.is_staff):
            msg = 'User account is not admin.'
            raise exceptions.ValidationError(msg)

        res_data = super(AdminLogin, self).post(request, *args, **kwargs)
        return res_data
