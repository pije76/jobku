from allauth.socialaccount.providers.facebook.views import FacebookOAuth2Adapter
from allauth.socialaccount.providers.linkedin_oauth2.views import LinkedInOAuth2Adapter
from rest_auth.registration.views import SocialLoginView
from allauth.socialaccount.providers.oauth2.client import OAuth2Client
from django.conf import settings
from rest_framework_tracking.mixins import LoggingMixin


class FacebookLogin(LoggingMixin, SocialLoginView):
    adapter_class = FacebookOAuth2Adapter
    sensitive_fields = {'access_token'}

class LinkedinLogin(LoggingMixin, SocialLoginView):
    adapter_class = LinkedInOAuth2Adapter
    client_class = OAuth2Client
    callback_url = settings.LINKEDIN_CALLBACK_URL
    sensitive_fields = {'code'}
