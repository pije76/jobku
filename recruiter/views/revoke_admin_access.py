from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import generics
from recruiter.models import Recruiter
from rest_framework_tracking.mixins import LoggingMixin


class RevokeAdminAccessView(LoggingMixin, generics.GenericAPIView):
    """
    Call this API to grant access to admin
    """
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        if "email" not in self.request.data:
            return Response(
                {
                    'code': '40',
                    'message': 'not enough parameters required',
                    'data': {'email': 'required'},
                }, status=400
            )
        if self.request.user.is_superuser is False:
            return Response(
                {
                    'code': '41',
                    'message': 'You have no access',
                    'data': {},
                }, status=400
            )
        email_target = self.request.data["email"]
        try:
            user = Recruiter.objects.get(email=email_target)
        except Recruiter.DoesNotExist:
            return Response(
                {
                    'code': '42',
                    'message': 'User does not exists',
                    'data': {},
                }, status=400
            )
        else:
            if self.request.user.email == email_target:
                return Response(
                    {
                        'code': '43',
                        'message': "You cannot revoke yourself",
                        'data': {},
                    }, status=400
                )
            else:
                user.is_staff = False
                user.save()

        return Response(
            {
                'code': '20',
                'message': 'Access revoked!',
                'data': {},
            }, status=200
        )
