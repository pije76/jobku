from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import generics
from django.db.models import Sum
from recruiter.models import Recruiter
from referral.models import Referral
from rest_framework_tracking.mixins import LoggingMixin


class RecruiterMetricView(LoggingMixin, generics.GenericAPIView):
    """
    Call this API to get the recruiter metric
    """
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        if self.request.user.is_staff is False:
            return Response(
                {
                    'code': '40',
                    'message': 'You have no access',
                    'data': {},
                }, status=400
            )

        data = {
            'recruiter_registered': Recruiter.objects.filter(is_superuser=False, is_staff=False).count(),
            'recruiter_active_referring': Referral.objects.filter(success=False).distinct('referrer').count(),
            'recruiter_successful_referring': Referral.objects.filter(success=True).distinct('referrer').count(),
            'top_recruiters_referring': Recruiter.objects.filter(is_superuser=False, is_staff=False).order_by('-referral_count').values('username', 'referral_count', 'referral_hit_count', 'last_login'),
            # 'top_recruiters_earning': self.get_serializer(Recruiter.objects.filter(is_superuser=False, is_staff=False).order_by('-commission'), many=True).data,
            # 'bottom_recruiters_referring': self.get_serializer(Recruiter.objects.filter(is_superuser=False, is_staff=False).order_by('referral_count'), many=True).data,
            # 'bottom_recruiters_earning': self.get_serializer(Recruiter.objects.filter(is_superuser=False, is_staff=False).order_by('commission'), many=True).data,
            # 'candidate_referred': Referral.objects.filter(success=False).count(),
            # 'candidate_hired': Referral.objects.filter(success=True).count(),
            # 'total_commission_earned': '???',
            # 'total_commission_paid': Recruiter.objects.filter(is_superuser=False, is_staff=False).aggregate(Sum('commission'))['commission__sum'],
        }

        return Response(
            {
                'code': '20',
                'message': 'Success',
                'data': data,
            }, status=200
        )
