import hashids
from django.conf import settings

HASH_IDS = hashids.Hashids(salt=settings.HASHIDS_SALT)