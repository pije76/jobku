# -*- coding: utf-8 -*-
# Generated by Django 1.11.14 on 2018-10-08 14:32
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recruiter', '0003_auto_20181003_1607'),
    ]

    operations = [
        migrations.AddField(
            model_name='recruiter',
            name='photo',
            field=models.ImageField(max_length=255, null=True, upload_to='recruiter/%Y/%m/%d/'),
        ),
    ]
