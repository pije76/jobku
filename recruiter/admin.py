from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models.recruiter import Recruiter, Blacklist

class CustomUserAdmin(UserAdmin):
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'email', 'password1', 'password2')}
        ),
    )

admin.site.register(Recruiter, CustomUserAdmin)
admin.site.register(Blacklist)
