from rest_framework.routers import SimpleRouter
from django.conf.urls import url, include
from rest_framework_jwt.views import verify_jwt_token
from recruiter.views.admin_login import AdminLogin
from recruiter.views.give_admin_access import GiveAdminAccessView
from recruiter.views.revoke_admin_access import RevokeAdminAccessView
from recruiter.views.profile_viewset import ProfileViewSet, RecruiterViewSet, ResendConfirmation
from recruiter.views.recruiter_metric import RecruiterMetricView
from recruiter.views.social_viewset import FacebookLogin, LinkedinLogin


route = SimpleRouter()
route.register('profile', ProfileViewSet, base_name='profile')
route.register('recruiter', RecruiterViewSet, base_name='recruiter')

urlpatterns = [
    url(r'^', include('django.contrib.auth.urls')),
    url(r'^auth/', include('custom_rest_auth.urls')),
    url(r'^auth/verify/', verify_jwt_token),
    url(r'^auth/registration/', include('custom_rest_auth.registration.urls')),
    # url(r'^auth/admin_login/$', AdminLogin.as_view(), name='admin_login'),
    # url(r'^open_admin_access/$', GiveAdminAccessView.as_view(), name='give_admin_access'),
    # url(r'^revoke_admin_access/$', RevokeAdminAccessView.as_view(), name='revoke_admin_access'),
    url(r'^recruiter_metric/$', RecruiterMetricView.as_view(), name='recruiter_metric'),
    url(r'^rest-auth/facebook/$', FacebookLogin.as_view(), name='fb_login'),
    url(r'^rest-auth/linkedin/$', LinkedinLogin.as_view(), name='linkedin_login'),
    url(r'^resend-confirmation/$', ResendConfirmation.as_view(), name='resend_confirmation'),
]

urlpatterns += route.urls
