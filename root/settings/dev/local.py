from root.settings.base import *  # noqa: F401, F403

MEDIA_ROOT = 'media'  # noqa: F405
MEDIA_URL = '/media/'
