import os

from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin

from rest_framework_swagger.views import get_swagger_view


schema_view = get_swagger_view(title='Recruiter API')

urlpatterns = [
    url(r'^admin/', include('admin_honeypot.urls', namespace='admin_honeypot')),
    url(r'^rmsadmin/', admin.site.urls),
    url(r'^recruiter/', include('allauth.urls')),
    url(r'^api/recruiter/', include('recruiter.api_urls.api_urls')),
    url(r'^api/job/', include('job.api_urls.api_urls')),
    url(r'^api/referral/', include('referral.api_urls.api_urls')),
    url(r'^docs/$', schema_view)
]

# TODO: we should change this to check the existence of settings.MEDIA_URL
if os.environ['DJANGO_SETTINGS_MODULE'] == 'root.settings.dev.local':
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
