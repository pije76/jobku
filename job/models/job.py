from django.db import models


class Job(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(default='')
    skill = models.ManyToManyField(to='job.Skill', blank=True)
    location = models.CharField(max_length=150, default='')
    salary_min = models.DecimalField(default=0, max_digits=8, decimal_places=2)
    salary_max = models.DecimalField(default=0, max_digits=8, decimal_places=2)
    company_name = models.CharField(max_length=150, default='')
    company_logo = models.ImageField(upload_to='job/companyLogo/%Y/%m/%d/', null=True, max_length=255)
    date_created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name