from rest_framework.routers import SimpleRouter
from job.views.job_viewset import JobViewSet


route = SimpleRouter()
route.register('', JobViewSet, base_name='job')

urlpatterns = [

]

urlpatterns += route.urls
