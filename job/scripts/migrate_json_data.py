from job.models import Job, Skill
import json
import os


def migrate_json_to_database():
    module_dir = os.path.dirname(__file__)  # get current directory
    file_path = os.path.join(module_dir, 'temp_job_data.json')
    with open(file_path, 'r+') as f:
        json_data = json.load(f)
        for data in json_data:
            job_obj = Job.objects.create(name=data["name"],
                                         description=data["description"],
                                         location=data["location"],
                                         salary_min=int(data["salary"][0]),
                                         salary_max=int(data["salary"][1]),
                                         company_name=data["company"]
                                         )
            skill_data = data["skill"]
            for skill in skill_data:
                try:
                    skill_obj = Skill.objects.get(name=skill)
                except Skill.DoesNotExist:
                    skill_obj = Skill.objects.create(name=skill)

                job_obj.skill.add(skill_obj)

            print(job_obj)
