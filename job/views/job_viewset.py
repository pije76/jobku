from rest_framework import viewsets, mixins
from job.models import Job
from job.serializers.job_serializer import JobSerializer


class JobViewSet(mixins.RetrieveModelMixin,
                     mixins.ListModelMixin,
                     viewsets.GenericViewSet):
    queryset = Job.objects.all()
    serializer_class = JobSerializer