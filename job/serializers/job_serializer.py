from rest_framework import serializers

from job.models import Job


class JobSerializer(serializers.ModelSerializer):
    skill = serializers.StringRelatedField(many=True)

    class Meta:
        model = Job
        fields = '__all__'
