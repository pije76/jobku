from rest_framework.permissions import IsAuthenticated, SAFE_METHODS

class IsAuthenticatedSafe(IsAuthenticated):

    def has_permission(self, request, view):
        if request.method in SAFE_METHODS:
            return True
        return super().has_permission(request, view)
