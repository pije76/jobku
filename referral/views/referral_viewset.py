from rest_framework import viewsets, mixins
from rest_framework.views import APIView
from referral.models import Referral
from referral.serializers.referral_serializer import ReferralSerializer, generate_url, ApplyReferralSerializer, UploadCvSerializer
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework_serializer_extensions.views import ExternalIdViewMixin
from django.conf import settings
from rest_framework_tracking.mixins import LoggingMixin
from rest_framework_serializer_extensions.utils import external_id_from_model_and_internal_id, internal_id_from_model_and_external_id
from referral.permissions import IsAuthenticatedSafe
from django.db.models.functions import TruncMonth, ExtractMonth
from django.db.models import Count
import requests


def safe_list_get(l, idx, default):
    try:
        return l[idx]
    except IndexError:
        return default

def safe_list_join(l):
    try:
        return ''.join(l)
    except TypeError:
        return None

class ReferralViewSet(LoggingMixin, ExternalIdViewMixin, viewsets.ModelViewSet):
    queryset = Referral.objects.all()
    permission_classes = (IsAuthenticatedSafe,)
    filterset_fields = ('job',)

    def get_serializer_class(self):
        if self.action == 'partial_update':
            return ApplyReferralSerializer
        return ReferralSerializer

    def get_queryset(self):
        if self.request.user.is_authenticated:
            return self.request.user.referral_set.all()
        return Referral.objects.all()

    def perform_create(self, serializer):
        serializer.save(recruiter=self.request.user)

class ReferralUrl(LoggingMixin, APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, job_id, format=None):
        url = generate_url(job_id, 1, recruiter_id=request.user.id)
        return Response({'referral_url': url})

class ApplyReferralViewSet(LoggingMixin, ExternalIdViewMixin, mixins.CreateModelMixin, mixins.UpdateModelMixin, viewsets.GenericViewSet):
    queryset = Referral.objects.all()
    serializer_class = ApplyReferralSerializer

    def send_candidate(self, instance):
        data = {
            'first_name': instance.first_name,
            'last_name': instance.last_name,
            'email': instance.email,
            'mobile_number': instance.contact_number,
            'skype': instance.skype
        }
        files = {
            'curriculum_vitae': instance.curriculum_vitae
        }
        try:
            requests.post(f'{settings.SECONDARY_API}candidate/', data=data, files=files)
        except ValueError:
            requests.post(f'{settings.SECONDARY_API}candidate/', data=data)

    def perform_create(self, serializer):
        instance = serializer.save()

        # send candidate data to secondary api
        self.send_candidate(instance)

    def perform_update(self, serializer):
        instance = serializer.save()

        # send candidate data to secondary api
        self.send_candidate(instance)

class UploadCv(LoggingMixin, APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        serializer = ReferralSerializer(request.user.referral_set.filter(type=0, cv_editing=True), many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        for cv in request.FILES.getlist('curriculum_vitae'):
            ref = request.user.referral_set.create(
                first_name='-',
                last_name='-',
                email='-',
                type=0,
                job=request.data['job'],
                curriculum_vitae=cv,
                cv_editing=True
            )

        serializer = UploadCvSerializer(request.user.referral_set.filter(type=0, cv_editing=True), many=True)
        return Response(serializer.data)

# class UploadCv(LoggingMixin, APIView):
#     permission_classes = (IsAuthenticated,)

#     def post(self, request, format=None):
#         cv_files = request.FILES.getlist('curriculum_vitae')

#         auth = requests.auth.HTTPBasicAuth(settings.CV_PARSER_USERNAME, settings.CV_PARSER_PASSWORD)

#         multiple_files = list()
#         for cv in cv_files:
#             data = ('curriculum_vitae', cv,)
#             multiple_files.append(data)

#         r = requests.post(f'{settings.SECONDARY_API}parse-cv/', auth=auth, files=multiple_files)
#         result = r.json()

#         if r.status_code == 200:
#             for obj in result:
#                 for cv in cv_files: # find cv file based on response file_name
#                     if obj['file_name'] == cv.name:
#                         file = cv
#                         break
#                 ref = request.user.referral_set.create(
#                     first_name=safe_list_get(obj['candidate_name'].split(), 0, ''),
#                     last_name=safe_list_get(obj['candidate_name'].split(), -1, ''),
#                     email=obj['email'],
#                     contact_number=safe_list_join(obj['phone']),
#                     type=0,
#                     job=request.data['job'],
#                     curriculum_vitae=file,
#                     parsed_data=obj
#                 )
#                 obj['ref_id'] = external_id_from_model_and_internal_id(Referral, ref.id)

#         return Response(result, status=r.status_code)

# class SubmitCv(LoggingMixin, APIView):
#     permission_classes = (IsAuthenticated,)

#     def patch(self, request, format=None):
#         for obj in request.data:
#             try:
#                 referral = Referral.objects.get(id=internal_id_from_model_and_external_id(Referral, obj['ref_id']))
#             except Referral.DoesNotExist:
#                 return Response({"message": f"Referral ID {obj['ref_id']} not found."}, status=400)

#             if referral:
#                 referral.updated_data = obj
#                 referral.save()

#         return Response({"message": "Success"})

class ReferralChart(LoggingMixin, APIView):

    def get(self, request, format=None):
        data = Referral.objects.filter(job__in=request.GET.get('job_in').split(',')).annotate(month=ExtractMonth('date_created')).values('month').annotate(count=Count('id')).values('month', 'count')
        return Response(data)
