from rest_framework import serializers
from referral.models import Referral
from rest_framework_serializer_extensions.fields import HashIdField
from django.conf import settings
from rest_framework_serializer_extensions.utils import external_id_from_model_and_internal_id, internal_id_from_model_and_external_id
from recruiter.models import Recruiter
from django.template import loader
from django.utils.html import strip_tags
from django.core.mail import send_mail
import requests, os


def generate_url(job_id, type, recruiter_id=None, referral_id=None):
    if type == 1: # url
        id_hash = external_id_from_model_and_internal_id(Recruiter, recruiter_id)
        param = 'recruiter'
    elif type == 2: # email
        id_hash = external_id_from_model_and_internal_id(Referral, referral_id)
        param = 'referral'
    return f'{settings.SITE_NAME}job/{job_id}?{param}={id_hash}&referral_type={type}'

def send_email(html_message, title, receiver):
    send_mail(
        title,
        strip_tags(html_message),
        settings.DEFAULT_FROM_EMAIL,
        receiver,
        fail_silently=False,
        html_message=html_message,
    )

class ReferralSerializer(serializers.ModelSerializer):
    id = HashIdField(model=Referral, read_only=True)
    status = serializers.SerializerMethodField()
    file_name = serializers.SerializerMethodField()

    def get_status(self, obj):
        return obj.get_status_display()

    def get_file_name(self, obj):
        try:
            return os.path.basename(obj.curriculum_vitae.file.name)
        except ValueError:
            return None

    # def validate(self, data):
    #     if Referral.objects.filter(email=data['email'], job=data['job'], recruiter=self.context['request'].user, type=data['type'], status=0).exists():
    #         raise serializers.ValidationError("You have referred this candidate on this job")
    #     return data

    def create(self, validated_data):
        instance = None
        title, html_message, receiver = [], [], [] # list initialization
        # get job name from secondary api
        r = requests.get(f"{settings.SECONDARY_API}job/{validated_data['job']}/")
        job_name = r.json()['name']

        if validated_data['type'] == 0: # manual
            if not Referral.objects.filter(email=self.initial_data['email'], job=validated_data['job'], recruiter=self.context['request'].user, type=validated_data['type'], status=0).exists():
                instance = super().create(validated_data)
                instance.first_name = self.initial_data['first_name']
                instance.last_name = self.initial_data['last_name']
                instance.email = self.initial_data['email']
                instance.save()

                # manual referral send email to recruiter, admin, candidate
                title.append('Referjobs Apply Job Invitation - Recruiter')
                html_message.append(loader.render_to_string('referral/refer_job_recruiter.html', {'recruiter': instance.recruiter}))
                receiver.append(instance.recruiter.email)
                title.append('Referjobs Apply Job Invitation - Admin')
                html_message.append(loader.render_to_string('referral/refer_job_admin.html', {'candidate': instance.first_name, 'recruiter': instance.recruiter, 'job': job_name}))
                receiver.append(settings.ADMIN_EMAIL)
                title.append('Referjobs Apply Job Invitation - Candidate')
                html_message.append(loader.render_to_string('referral/refer_job_candidate.html', {'candidate': instance.first_name, 'recruiter': instance.recruiter, 'job': job_name, 'url': None}))
                receiver.append(instance.email)

                for i, val in enumerate(html_message):
                    send_email(html_message[i], title[i], [receiver[i]])

        elif validated_data['type'] == 2: # by email
            for email in self.initial_data['emails']:
                if Referral.objects.filter(email=email['email'], job=validated_data['job'], recruiter=self.context['request'].user, type=validated_data['type'], status=0).exists():
                    continue

                instance = super().create(validated_data)
                instance.first_name = email['first_name']
                instance.last_name = email['last_name']
                instance.email = email['email']
                instance.relation = email['relation']
                instance.save()

                # generate referral url
                url = generate_url(validated_data['job'], 2, referral_id=instance.id)

                # referral by email only send email to candidate
                title = 'Referjobs Apply Job Invitation - Candidate'
                html_message = loader.render_to_string('referral/refer_job_candidate.html', {'candidate': instance.first_name, 'recruiter': instance.recruiter, 'job': job_name, 'url': url})
                receiver = instance.email

                send_email(html_message, title, [receiver])

        if instance:
            return instance
        raise serializers.ValidationError("You have referred this candidate(s) on this job")

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        try:
            include = self.context.get('request', None).GET.get('include', False)
        except AttributeError:
            return ret
        if include:
            if 'job' in include:
                payload = {'include': 'company'}
                ret['job'] = requests.get(f"{settings.SECONDARY_API}job/{ret['job']}/", params=payload).json()
        return ret

    class Meta:
        model = Referral
        exclude = ('parsed_data', 'updated_data', 'updated_date',)
        read_only_fields = ('recruiter', 'first_name', 'last_name', 'email')

class ApplyReferralSerializer(serializers.ModelSerializer):
    id = HashIdField(model=Referral, read_only=True)

    def validate(self, data):
        if data['type'] == 1:
            if Referral.objects.filter(email=data['email'], job=data['job'], recruiter_id=internal_id_from_model_and_external_id(Recruiter, self.initial_data['recruiter']), type=1, status=0, applied=True).exists():
                raise serializers.ValidationError("You have applied for this job")
        elif data['type'] == 2:
            if Referral.objects.filter(id=self.instance.id, status=0, applied=True).exists():
                raise serializers.ValidationError("You have applied for this job")
        return data

    def create(self, validated_data):
        validated_data['recruiter_id'] = internal_id_from_model_and_external_id(Recruiter, self.initial_data['recruiter'])
        validated_data['applied'] = True
        return super().create(validated_data)

    def update(self, instance, validated_data):
        validated_data['applied'] = True
        return super().update(instance, validated_data)

    class Meta:
        model = Referral
        fields = '__all__'
        read_only_fields = ('recruiter',)

class UploadCvSerializer(serializers.ModelSerializer):
    id = HashIdField(model=Referral, read_only=True)
    file_name = serializers.SerializerMethodField()

    def get_file_name(self, obj):
        try:
            return os.path.basename(obj.curriculum_vitae.file.name)
        except ValueError:
            return None

    class Meta:
        model = Referral
        fields = ('id', 'file_name',)
