# -*- coding: utf-8 -*-
# Generated by Django 1.11.14 on 2019-03-13 10:16
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('referral', '0011_referral_applied'),
    ]

    operations = [
        migrations.RenameField(
            model_name='referral',
            old_name='name',
            new_name='first_name',
        ),
        migrations.AddField(
            model_name='referral',
            name='last_name',
            field=models.CharField(default='', max_length=50),
            preserve_default=False,
        ),
    ]
