# -*- coding: utf-8 -*-
# Generated by Django 1.11.14 on 2018-10-20 13:55
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('referral', '0008_auto_20181019_1639'),
    ]

    operations = [
        migrations.AddField(
            model_name='referral',
            name='date_hired',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='referral',
            name='status',
            field=models.IntegerField(choices=[(0, 'Considering'), (1, 'Interviewed'), (2, 'Rejected'), (3, 'Hired')], default=0),
        ),
    ]
