# -*- coding: utf-8 -*-
# Generated by Django 1.11.14 on 2019-04-11 07:48
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('referral', '0017_referral_gender'),
    ]

    operations = [
        migrations.AddField(
            model_name='referral',
            name='edited',
            field=models.BooleanField(default=False),
        ),
    ]
