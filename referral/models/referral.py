from django.db import models
from django.core.validators import RegexValidator
from django.contrib.postgres.fields import ArrayField, JSONField
from django.conf import settings
from django.core.mail import send_mail
from django.template import loader
from django.utils.html import strip_tags
from recruiter.models.recruiter import Recruiter
import referral.serializers as r_serializer # avoid circular import


GENDER = (
    (0, 'Male'),
    (1, 'Female'),
)

EXPERIENCE = (
    (0, 'Fresher (0-1 year)'),
    (1, 'Junior (1-2 year)'),
    (2, 'Mid-level (2-3 year)'),
    (3, 'Senior (3-5 year)'),
)

REFERRAL_TYPE = (
    (0, 'Manual'),
    (1, 'Url'),
    (2, 'Email'),
    (3, 'Linkedin'),
    (4, 'Facebook'),
)

STATUS = (
    (0, 'Considering'),
    (1, 'Interviewed'),
    (2, 'Rejected'),
    (3, 'Hired'),
    (4, 'Screened'),
    (5, 'Offered'),
)

class Referral(models.Model):
    recruiter = models.ForeignKey(Recruiter, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    email = models.CharField(max_length=100)
    relation = models.CharField(max_length=100, blank=True, null=True)
    city = models.CharField(max_length=100, blank=True, null=True)
    country = models.CharField(max_length=100, blank=True, null=True)
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Wrong Format")
    contact_number = models.CharField(validators=[phone_regex], max_length=17, blank=True, null=True)
    gender = models.IntegerField(null=True, choices=GENDER)
    date_of_birth = models.DateField(blank=True, null=True)
    address = models.TextField(blank=True)
    website = models.CharField(max_length=100, blank=True, null=True)
    experience = models.IntegerField(null=True, choices=EXPERIENCE)
    skype = models.CharField(max_length=100, blank=True, null=True)
    nationality = models.CharField(max_length=100, blank=True, null=True)
    facebook = models.CharField(max_length=100, blank=True, null=True)
    linkedin = models.CharField(max_length=100, blank=True, null=True)
    twitter = models.CharField(max_length=100, blank=True, null=True)
    education = JSONField(blank=True, null=True)
    working_experience = JSONField(blank=True, null=True)
    portfolio = ArrayField(models.CharField(max_length=255), blank=True, null=True)
    skill = ArrayField(models.CharField(max_length=50, blank=True, null=True), null=True)
    interest = ArrayField(models.CharField(max_length=50, blank=True, null=True), null=True)
    references = JSONField(blank=True, null=True)
    notice_period = models.IntegerField(null=True)
    reason_referred = models.TextField(blank=True, null=True)
    reason_leave_job = models.TextField(blank=True, null=True)
    start_date = models.DateField(max_length=8, blank=True, null=True)
    current_salary = models.DecimalField(null=True, max_digits=8, decimal_places=2)
    speaking_level = models.IntegerField(null=True)
    writing_level = models.IntegerField(null=True)
    expected_salary = models.DecimalField(null=True, max_digits=8, decimal_places=2)
    curriculum_vitae = models.FileField(upload_to='curriculum_vitae/%Y/%m/%d/', blank=True, null=True, max_length=255)
    type = models.IntegerField(default=0, choices=REFERRAL_TYPE)
    status = models.IntegerField(default=0, choices=STATUS)
    date_created = models.DateTimeField(auto_now_add=True)
    date_hired = models.DateTimeField(null=True, blank=True)
    job = models.CharField(max_length=50)
    applied = models.BooleanField(default=False)
    parsed_data = JSONField(blank=True, null=True)
    updated_data = JSONField(blank=True, null=True)
    cv_editing = models.BooleanField(default=False)
    updated_date = models.DateTimeField(auto_now=True)

    __original_status = None

    def __init__(self, *args, **kwargs):
        super(Referral, self).__init__(*args, **kwargs)
        self.__original_status = self.status

    def __str__(self):
        return f'[{self.get_status_display()}] {self.first_name} {self.last_name} ({self.job}) - referred by {self.recruiter}'

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        if self.status != self.__original_status and self.status == 3: # if referral hired then send email
            r_serializer.send_email(loader.render_to_string('referral/refer_job_success.html'), 'Referjobs - Referral is Hired', [self.recruiter.email, settings.ADMIN_EMAIL])
        self.__original_status = self.status
