from rest_framework.routers import SimpleRouter
from referral.views.referral_viewset import ReferralViewSet, ReferralUrl, ApplyReferralViewSet, UploadCv, ReferralChart
from django.conf.urls import url


route = SimpleRouter()
route.register('apply', ApplyReferralViewSet, base_name='apply')
route.register('', ReferralViewSet, base_name='referral')

urlpatterns = [
    url(r'^referral-url/(?P<job_id>\w+)/$', ReferralUrl.as_view()),
    url(r'^upload-cv/$', UploadCv.as_view()),
    # url(r'^submit-cv/$', SubmitCv.as_view()),
    url(r'^referral-chart/$', ReferralChart.as_view()),
]

urlpatterns += route.urls